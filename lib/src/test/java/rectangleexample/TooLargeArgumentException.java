package com.wsu.cs;

/*
Custom exception class - TooLargeArgumentException
 extends IllegalArgumentException
 */

public class TooLargeArgumentException extends IllegalArgumentException {

    public TooLargeArgumentException() {
        super("Argument is too large");
    }
}
