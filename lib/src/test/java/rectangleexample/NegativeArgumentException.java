package com.wsu.cs;

/*
Custom exception class - NegativeArgumentException
 extends IllegalArgumentException
 */

public class NegativeArgumentException extends IllegalArgumentException {

    public NegativeArgumentException() {
        super("Argument cannot be negative");
    }
}
