package com.wsu.cs;

/*
 * POGIL Activity 3 and 4 - Testing with JUnit5
 * RectangleTest Example
 * @author S Nagpal
 * @version Spring 2021
 */

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



public class RectangleTest {


    /*
     Test method for method getArea()
    */
    @Test
    void testRectangleArea() {
        Rectangle r1 = new Rectangle(2, 3);
        int area = r1.getArea();
        assertEquals(6, area);
    }

    /*
     Test method for method isSquare()
    */
    @Test
    void testRectangleNotSquare() {
        Rectangle r1 = new Rectangle(2, 3);
        boolean isSquare = r1.isSquare();
        assertFalse(isSquare);
    }

    /*
     Test method for two Rectangles not same()
    */

    @Test
    void testRectangleNotSame() {
        Rectangle r1 = new Rectangle(2, 3);
        Rectangle r2 = new Rectangle(4, 5);
        assertNotSame(r1, r2, "Rectangles not Same");
    }

    /*
     Test method with IllegalArgumentException -- POGIL Activity 4 - Model 1
    */
    @Test
    void testThrowsExceptionOnNegativeSides() {
        assertThrows(IllegalArgumentException.class,
                () -> { new Rectangle(2,-1); } );
    }

    /*
     Test method with IllegalArgumentException -- POGIL Activity 4 - Models 2 and 3
    */
    @Test
    void testThrowsCustomExceptions() {
        assertAll(
                () -> { Exception e =
                        assertThrows(IllegalArgumentException.class,
                                () -> { new Rectangle(2,-1); } );
                    assertEquals("Argument cannot be negative",
                            e.getMessage()); },
                () -> { Exception e =
                        assertThrows(TooLargeArgumentException.class,
                                () -> { new Rectangle(2,200); } );
                    assertEquals("Argument is too large",
                            e.getMessage()); } );
    }

}