package com.wsu.cs;

/*
 * POGIL Activity 3 and 4 - Testing with JUnit5
 * Rectangle Example
 * @author S Nagpal
 * @version Spring 2021
 */

/*
Rectangle class
instance variables: int height - height of the rectangle
                    int width - width of the rectangle
 */

public class Rectangle {

    private int height;
    private int width;

	/*
	//Constructor per POGIL Activity 3

	public Rectangle(int height, int width) {
		this.height = height;
		this.width = width;
	}
	*/

	/*
	//Constructor per POGIL Activity 4 - Model 1

	public Rectangle(int height, int width) throws IllegalArgumentException
	{
		if (height <=0 || width <= 0)
		{
			throw new IllegalArgumentException();
		}
		this.height = height;
		this.width = width;
	}
	*/

    // Constructor per POGIL Activity 4 - Model 2 and 3 with Custom exceptions
    public Rectangle(int height, int width) throws IllegalArgumentException
    {
        if (height <=0 || width <= 0)
        {
            throw new NegativeArgumentException();
        }
        else
        if (height > 100 || width > 100)
        {
            throw new TooLargeArgumentException();
        }
        this.height = height;
        this.width = width;
    }

    /*
    Get the area of the rectangle
    @return area of the rectangle
     */
    public int getArea() {
        return height * width;
    }

    /*
    Determine if the rectangle is a square.
    @return true if the rectangle is a square
     */

    public boolean isSquare() {
        return height == width;
    }


}

